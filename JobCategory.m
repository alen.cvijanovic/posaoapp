//
//  JobCategory.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "JobCategory.h"
#import "Job.h"


@implementation JobCategory

@dynamic categoryTitle;
@dynamic categoryURL;
@dynamic categoryID;
@dynamic categorySelected;
@dynamic categoryLoaded;
@dynamic categoryLoadedTime;
@dynamic toJob;

@end
