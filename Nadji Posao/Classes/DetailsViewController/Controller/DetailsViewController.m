//
//  DetailsViewController.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "DetailsViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "DataSource.h"
#import "TUSafariActivity.h"
#import <LReachabilityController.h>
#import <Parse/Parse.h>


@interface DetailsViewController ()
{
    UIVisualEffectView *_blurEffectView;
    UITapGestureRecognizer *_tapGestureRecognizer;
    UIView *_blurView;
}

@end

@implementation DetailsViewController


#pragma mark - loadGUI


- (void)loadGUI
{
    [super loadGUI];

    NSString *codeString = [NSString stringWithFormat:@"%@", _job.jobTitle];
    [PFAnalytics trackEvent:@"jobOpened" dimensions:@{ @"title": codeString }];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
    _webView.backgroundColor = [UIColor darkGrayColor];

    self.title = @"Učitavanje...";
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"info"] style:UIBarButtonItemStylePlain target:self action:@selector(infoButtonPressed)];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    _verticalSpaceForLinkImageView.constant = _verticalSpaceForLinkImageView.constant - 140;
    _verticalSpaceForScreenshotImageView.constant = _verticalSpaceForScreenshotImageView.constant - 120;
    _horizontalSpaceForCopyLinkLabel.constant = _horizontalSpaceForCopyLinkLabel.constant - 110;
    _horizontalSpaceForScreenshotLabel.constant = _horizontalSpaceForScreenshotLabel.constant - 160;
    _centerConstraintLabelOne.constant = _centerConstraintLabelOne.constant + 350;
    _centerConstraintLabelTwo.constant = _centerConstraintLabelTwo.constant - 350;
    _centerConstraintLabelThree.constant = _centerConstraintLabelThree.constant + 350;
    _centerConstraintLabelFour.constant = _centerConstraintLabelFour.constant - 350;
    _textOneLabel.hidden = _textThreeLabel.hidden = YES;
    
    [self.view layoutIfNeeded];
    
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapRecognized)];
    
    _screenShotButton.enabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged)
                                                 name:kReachabilityControllerStatusChanged
                                               object:nil];
    
    _shareButton.enabled = [[LReachabilityController sharedReachabilityController] internetAvailable];
    
    _toolbar.backgroundColor = [UIColor blackColor];
}


#pragma mark - bindGUI


- (void)bindGUI
{
    NSURL *myURL = [NSURL URLWithString: [_job.jobURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if (_job.toFavorite)
    {
        [_favoriteButton setImage:[UIImage imageNamed:@"favorite_Filled"]];
        _favoriteButton.tag = 1;
        
    }
    else
    {
        [_favoriteButton setImage:[UIImage imageNamed:@"favorite"]];
        _favoriteButton.tag = 0;
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:myURL];
    [_webView loadRequest:request];
}


#pragma mark - viewWillAppear


- (void)viewWillAppear:(BOOL)animated
{
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
}


#pragma mark - viewWillDisappear


- (void)viewWillDisappear:(BOOL)animated
{
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
}


#pragma mark - WebView Delegate methods


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    int64_t delayInMiliseconds = 500;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInMiliseconds * NSEC_PER_MSEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.title = @"Detalji";
            });
    
    if ([webView.request.URL.absoluteString rangeOfString:@"OglasIstekao"].length > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Greška" message:@"Ups! Traženi oglas je istekao!\nOvaj oglas će automatski biti obrisan!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        alert.tag = 279;
        
        [alert show];
        
        [[DataSource sharedDataSource] deleteJob:_job];
        
        return;
    }
    
    [webView.scrollView setContentSize: CGSizeMake(webView.frame.size.width, webView.scrollView.contentSize.height)];
    
    _screenShotButton.enabled = YES;
    
    [[DataSource sharedDataSource] jobSeen:_job];
}


#pragma mark - Actions


- (IBAction)shareButtonPressed:(id)sender
{
    NSString *string = [_job.jobTitle capitalizedString];
    NSURL *URL = [NSURL URLWithString:_job.jobURL];
    UIImage *image = [self imageFromWebview:_webView];
    TUSafariActivity *activity = [[TUSafariActivity alloc] init];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[string, URL, image]
                                      applicationActivities:@[activity]];

    NSArray *excludeActivities = @[UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityViewController.excludedActivityTypes = excludeActivities;
    
    
    [self.navigationController presentViewController:activityViewController
                                       animated:YES
                                     completion:^{
                                         [_webView.scrollView setContentSize: CGSizeMake(_webView.frame.size.width, _webView.scrollView.contentSize.height)];
                                     }];
}


- (IBAction)favoriteButtonPressed:(id)sender
{
    if (_favoriteButton.tag == 0)
    {
        [_favoriteButton setImage:[UIImage imageNamed:@"favorite_Filled"]];
        _favoriteButton.tag = 1;
        [[DataSource sharedDataSource] addFavoriteToJob:_job];
        NSString *codeString = [NSString stringWithFormat:@"%@", _job.jobTitle];
        [PFAnalytics trackEvent:@"addedToFavorites" dimensions:@{ @"title": codeString }];
    }
    else
    {
        [_favoriteButton setImage:[UIImage imageNamed:@"favorite"]];
        _favoriteButton.tag = 0;
        [[DataSource sharedDataSource] removeFavoriteJob:_job];
    }
}


- (IBAction)screenshotButtonPressed:(id)sender
{
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    
    
    if (status == ALAuthorizationStatusNotDetermined)
    {
        UIImageWriteToSavedPhotosAlbum([self imageFromWebview:_webView],
                                       self,
                                       @selector(image:didFinishSavingWithError:contextInfo:),
                                       nil);
    }
    else if (status != ALAuthorizationStatusAuthorized || status == ALAuthorizationStatusDenied)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Molim omogućite ovoj aplikaciji pristup Vašoj photo galeriji.\n Ovu opciju možete promijeniti u aplikaciji Postavke" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alert show];
    }
    else
    {
        UIImageWriteToSavedPhotosAlbum([self imageFromWebview:_webView],
                                       self,
                                       @selector(image:didFinishSavingWithError:contextInfo:),
                                       nil);
    }
}


- (void)infoButtonPressed
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        if (_blurView)
        {
            self.navigationItem.rightBarButtonItem.enabled = NO;
            _verticalSpaceForLinkImageView.constant = _verticalSpaceForLinkImageView.constant - 140;
            _verticalSpaceForScreenshotImageView.constant = _verticalSpaceForScreenshotImageView.constant - 120;
            _horizontalSpaceForCopyLinkLabel.constant = _horizontalSpaceForCopyLinkLabel.constant - 110;
            _horizontalSpaceForScreenshotLabel.constant = _horizontalSpaceForScreenshotLabel.constant - 160;
            _centerConstraintLabelOne.constant = _centerConstraintLabelOne.constant + 350;
            _centerConstraintLabelTwo.constant = _centerConstraintLabelTwo.constant - 350;
            _centerConstraintLabelThree.constant = _centerConstraintLabelThree.constant + 350;
            _centerConstraintLabelFour.constant = _centerConstraintLabelFour.constant - 350;
            
            [UIView animateWithDuration:0.5 animations:^ {
                [self.view layoutIfNeeded];
                
            }
                             completion:^(BOOL finished){
                                 [UIView transitionWithView:self.view duration:0.3
                                                    options:UIViewAnimationOptionTransitionCrossDissolve
                                                 animations:^ {
                                                     _textOneLabel.hidden = _textThreeLabel.hidden = YES;
                                                     [_blurView removeFromSuperview];
                                                     _blurView = nil;
                                                     _toolbar.userInteractionEnabled = YES;
                                                     self.navigationItem.rightBarButtonItem.enabled = YES;
                                                 }
                                                 completion:nil];
                             }];
        }
        else
        {
            _toolbar.userInteractionEnabled = NO;
            
            _blurView = [[UIView alloc] initWithFrame:_webView.frame];
            [_blurView setBackgroundColor:[[UIColor darkTextColor] colorWithAlphaComponent:0.98]];
            
            [UIView transitionWithView:self.view duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^ {
                                [_webView addSubview:_blurView];
                                [_blurView addGestureRecognizer:_tapGestureRecognizer];
                            }
                            completion:^(BOOL finished){
                                _textOneLabel.hidden = _textThreeLabel.hidden = NO;
                                _verticalSpaceForLinkImageView.constant = _verticalSpaceForLinkImageView.constant + 140;
                                _verticalSpaceForScreenshotImageView.constant = _verticalSpaceForScreenshotImageView.constant + 120;
                                _horizontalSpaceForCopyLinkLabel.constant = _horizontalSpaceForCopyLinkLabel.constant + 110;
                                _horizontalSpaceForScreenshotLabel.constant = _horizontalSpaceForScreenshotLabel.constant + 160;
                                _centerConstraintLabelOne.constant = _centerConstraintLabelOne.constant - 350;
                                _centerConstraintLabelTwo.constant = _centerConstraintLabelTwo.constant + 350;
                                _centerConstraintLabelThree.constant = _centerConstraintLabelThree.constant - 350;
                                _centerConstraintLabelFour.constant = _centerConstraintLabelFour.constant + 350;
                                [UIView animateWithDuration:0.5 animations:^{
                                    [self.view layoutIfNeeded];
                                }];
                            }];
        }
    }
    else
    {
        if (_blurEffectView)
        {
            self.navigationItem.rightBarButtonItem.enabled = NO;
            _verticalSpaceForLinkImageView.constant = _verticalSpaceForLinkImageView.constant - 140;
            _verticalSpaceForScreenshotImageView.constant = _verticalSpaceForScreenshotImageView.constant - 120;
            _horizontalSpaceForCopyLinkLabel.constant = _horizontalSpaceForCopyLinkLabel.constant - 110;
            _horizontalSpaceForScreenshotLabel.constant = _horizontalSpaceForScreenshotLabel.constant - 160;
            _centerConstraintLabelOne.constant = _centerConstraintLabelOne.constant + 350;
            _centerConstraintLabelTwo.constant = _centerConstraintLabelTwo.constant - 350;
            _centerConstraintLabelThree.constant = _centerConstraintLabelThree.constant + 350;
            _centerConstraintLabelFour.constant = _centerConstraintLabelFour.constant - 350;
            
            [UIView animateWithDuration:0.5 animations:^ {
                [self.view layoutIfNeeded];
                
            }
                             completion:^(BOOL finished){
                                 [UIView transitionWithView:self.view duration:0.3
                                                    options:UIViewAnimationOptionTransitionCrossDissolve
                                                 animations:^ {
                                                     _textOneLabel.hidden = _textThreeLabel.hidden = YES;
                                                     [_blurEffectView removeFromSuperview];
                                                     _blurEffectView = nil;
                                                     _toolbar.userInteractionEnabled = YES;
                                                     self.navigationItem.rightBarButtonItem.enabled = YES;
                                                 }
                                                 completion:nil];
                             }];
        }
        else
        {
            _toolbar.userInteractionEnabled = NO;
            [self applyBlurToView:_webView withEffectStyle:UIBlurEffectStyleDark andConstraints:YES];
        }
    }
}


#pragma mark - Helpers


- (UIImage*)imageFromWebview:(UIWebView*)webview
{
    CGRect originalFrame = webview.frame;

    CGFloat webViewHeight = [[webview stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"] integerValue];
    [webview setFrame:CGRectMake(0, 44, self.view.frame.size.width, webViewHeight)];

    UIGraphicsBeginImageContextWithOptions(webview.frame.size, false, 0.0);
    [webview.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [webview setFrame:originalFrame];

    return image;
}


- (UIView *)applyBlurToView:(UIView *)view withEffectStyle:(UIBlurEffectStyle)style andConstraints:(BOOL)addConstraints
{
    
    if (UIAccessibilityIsReduceTransparencyEnabled != NULL)
    {
        if (!UIAccessibilityIsReduceTransparencyEnabled())
        {
            UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:style];
            _blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
            _blurEffectView.frame = view.bounds;
            
            [UIView transitionWithView:self.view duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^ {
                                [view addSubview:_blurEffectView];
                                [_blurEffectView addGestureRecognizer:_tapGestureRecognizer];
                            }
                            completion:^(BOOL finished){
                                _textOneLabel.hidden = _textThreeLabel.hidden = NO;
                                _verticalSpaceForLinkImageView.constant = _verticalSpaceForLinkImageView.constant + 140;
                                _verticalSpaceForScreenshotImageView.constant = _verticalSpaceForScreenshotImageView.constant + 120;
                                _horizontalSpaceForCopyLinkLabel.constant = _horizontalSpaceForCopyLinkLabel.constant + 110;
                                _horizontalSpaceForScreenshotLabel.constant = _horizontalSpaceForScreenshotLabel.constant + 160;
                                _centerConstraintLabelOne.constant = _centerConstraintLabelOne.constant - 350;
                                _centerConstraintLabelTwo.constant = _centerConstraintLabelTwo.constant + 350;
                                _centerConstraintLabelThree.constant = _centerConstraintLabelThree.constant - 350;
                                _centerConstraintLabelFour.constant = _centerConstraintLabelFour.constant + 350;
                                [UIView animateWithDuration:0.5 animations:^{
                                    [self.view layoutIfNeeded];
                                }];
                            }];
            
            if(addConstraints)
            {
                [_blurEffectView setTranslatesAutoresizingMaskIntoConstraints:NO];
                
                [view addConstraint:[NSLayoutConstraint constraintWithItem:_blurEffectView
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1
                                                                  constant:0]];
                
                [view addConstraint:[NSLayoutConstraint constraintWithItem:_blurEffectView
                                                                 attribute:NSLayoutAttributeBottom
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1
                                                                  constant:0]];
                
                [view addConstraint:[NSLayoutConstraint constraintWithItem:_blurEffectView
                                                                 attribute:NSLayoutAttributeLeading
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeLeading
                                                                multiplier:1
                                                                  constant:0]];
                
                [view addConstraint:[NSLayoutConstraint constraintWithItem:_blurEffectView
                                                                 attribute:NSLayoutAttributeTrailing
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeTrailing
                                                                multiplier:1
                                                                  constant:0]];
            }

        }
        else
        {
            view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
        }
    }
    return view;
}


- (void)singleTapRecognized
{
    [self infoButtonPressed];
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Molim omogućite ovoj aplikaciji pristup Vašoj photo galeriji.\n Ovu opciju možete promijeniti u aplikaciji Postavke" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alert show];
    }
    else
    {
       NSString *message = @"Oglas je uspješno sačuvan u Galeriju.";
        
        UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil, nil];
        [toast show];
        
        int duration = 2;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [toast dismissWithClickedButtonIndex:0 animated:YES];
            [_webView.scrollView setContentSize: CGSizeMake(_webView.frame.size.width, _webView.scrollView.contentSize.height)];
        });
    }
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 279)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)reachabilityChanged
{
    _shareButton.enabled = [[LReachabilityController sharedReachabilityController] internetAvailable];
}


@end
