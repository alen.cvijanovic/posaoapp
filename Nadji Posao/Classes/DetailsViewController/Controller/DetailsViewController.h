//
//  DetailsViewController.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "Job.h"

@interface DetailsViewController : AbstractViewController < UIWebViewDelegate, UIAlertViewDelegate >
{
    __weak IBOutlet UIWebView *_webView;
    __weak IBOutlet UIBarButtonItem *_favoriteButton;
    __weak IBOutlet UIBarButtonItem *_shareButton;

    __weak IBOutlet NSLayoutConstraint *_verticalSpaceForLinkImageView;
    __weak IBOutlet NSLayoutConstraint *_verticalSpaceForScreenshotImageView;
    __weak IBOutlet NSLayoutConstraint *_horizontalSpaceForCopyLinkLabel;
    __weak IBOutlet NSLayoutConstraint *_horizontalSpaceForScreenshotLabel;
    __weak IBOutlet NSLayoutConstraint *_centerConstraintLabelOne;
    __weak IBOutlet NSLayoutConstraint *_centerConstraintLabelTwo;
    __weak IBOutlet NSLayoutConstraint *_centerConstraintLabelThree;
    __weak IBOutlet NSLayoutConstraint *_centerConstraintLabelFour;
    
    __weak IBOutlet UILabel *_textOneLabel;
    __weak IBOutlet UILabel *_textThreeLabel;
    __weak IBOutlet UIToolbar *_toolbar;
    __weak IBOutlet UIBarButtonItem *_screenShotButton;
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;

@property (nonatomic, strong) Job *job;

@end
