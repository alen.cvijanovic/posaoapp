//
//  FavoritesTableViewCell.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Job.h"

@interface FavoritesTableViewCell : UITableViewCell
{
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UILabel *_jobLocationLabel;
    __weak IBOutlet UILabel *_jobEndDateLabel;
    __weak IBOutlet UIImageView *_seenImageView;
    __weak IBOutlet UIView *_innerContentView;
}


@property (nonatomic, strong) Job *job;
@property (nonatomic) BOOL showingFavorites;

@end
