//
//  FavoritesTableViewCell.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "FavoritesTableViewCell.h"
#import "NSDate+Utilities.h"

@implementation FavoritesTableViewCell

- (void)awakeFromNib {
   
    [_titleLabel setPreferredMaxLayoutWidth:[[UIScreen mainScreen] bounds].size.width - 20];
//    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
//    self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
//    _innerContentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setJob:(Job *)job
{
    _seenImageView.hidden = YES;
    _job = job;
    _titleLabel.text = job.jobTitle;
    _jobLocationLabel.text = job.jobLocation;
    [_jobLocationLabel setAdjustsFontSizeToFitWidth:YES];
    _jobEndDateLabel.text = [_job.jobEndDate stringWithFormat:@"dd.MM.yyyy"];
    
    if ([job toFavorite])
    {
        _seenImageView.hidden = NO;
        [_seenImageView setImage:[UIImage imageNamed:@"favorite_Filled"]];
    }
    else if ([job toHistory])
    {
        _seenImageView.hidden = NO;
        [_seenImageView setImage:[UIImage imageNamed:@"seen"]];
    }
}


- (void)setShowingFavorites:(BOOL)showingFavorites
{
    _seenImageView.hidden = showingFavorites;
}

@end
