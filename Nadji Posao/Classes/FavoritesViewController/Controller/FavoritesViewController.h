//
//  FavoritesViewController.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 08/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"

@interface FavoritesViewController : AbstractViewController < UITableViewDataSource, UITableViewDelegate >
{
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UILabel *_noItemsLabel1;
    __weak IBOutlet UILabel *_noItemsLabel2;
    __weak IBOutlet UILabel *_noItemsLabel3;
}

@end
