//
//  FavoritesViewController.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 08/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "FavoritesViewController.h"
#import "FavoritesTableViewCell.h"
#import "DataSource.h"
#import "DetailsViewController.h"
#import <LReachabilityController.h>
#import <Parse/Parse.h>

@interface FavoritesViewController ()
{
    NSArray *_dataArray;
    BOOL _loadHistory;
}

@end

@implementation FavoritesViewController


#pragma mark - loadGUI


- (void)loadGUI
{
    [super loadGUI];
    [self setupLeftMenuButton];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
    _tableView.backgroundColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Bookmark"] style:UIBarButtonItemStylePlain target:self action:@selector(bookmarkPressed:)];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    _tableView.alwaysBounceVertical = YES;

    [PFAnalytics trackEvent:@"Favorites opened"];
}


#pragma mark - loadData


- (void)loadData
{
    
    if (_loadHistory)
    {
         _dataArray = [[DataSource sharedDataSource] getHistoryJobs];
        self.title = @"Povijest gledanja";
        _noItemsLabel1.text = @"NEMA UNOSA";
        _noItemsLabel2.text = @"Prilikom svakog pregleda oglasa";
        _noItemsLabel3.text = @"oglas se automatski sprema u povijest";
        
        
    }
    else
    {
         _dataArray = [[DataSource sharedDataSource] getFavorites];
        self.title = @"Favoriti";
        _noItemsLabel1.text = @"NEMA UNOSA";
        _noItemsLabel2.text = @"Za spremanje oglasa u favorite";
        _noItemsLabel3.text = @"u prikazu oglasa odaberite zvjezdicu";
        
    }
    
    _tableView.userInteractionEnabled = [_dataArray count];
    
    _noItemsLabel1.hidden = _noItemsLabel2.hidden = _noItemsLabel3.hidden = [_dataArray count];
   
}


#pragma mark - bindGUI


- (void)bindGUI
{
    [self layoutGUI];
}


#pragma mark - layoutGUI


- (void)layoutGUI
{
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


#pragma mark - viewWillAppear


- (void)viewWillAppear:(BOOL)animated
{
    [self loadData];
    [_tableView reloadData];
}


#pragma mark - setupLeftMenuButton


- (void)setupLeftMenuButton
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Nazad" style:UIBarButtonItemStylePlain target:self action:@selector(backPressed:)];
    self.navigationItem.leftBarButtonItem = backButton;
}


#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *favoriteCustomCellIdentifier = @"FavoriteViewCustomCell";
    
    FavoritesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:favoriteCustomCellIdentifier];
    
    if (cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"FavoritesTableViewCell" bundle:nil] forCellReuseIdentifier:favoriteCustomCellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:favoriteCustomCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(FavoritesTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.job = [_dataArray objectAtIndex:indexPath.row];
    cell.showingFavorites = YES;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 141.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[LReachabilityController sharedReachabilityController] internetAvailable] || [[[_dataArray objectAtIndex:indexPath.row] jobSeen] boolValue])
    {
        DetailsViewController *detailsViewController = [DetailsViewController new];
        detailsViewController.job = [_dataArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:detailsViewController animated:YES];
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Uređaj nije spojen na internet. Molim provjerite vašu internet konekciju." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alertView show];
    }
    
}


#pragma mark - Actions


- (void)bookmarkPressed:(UIBarButtonItem *)button
{
    _loadHistory = !_loadHistory;
    [self loadData];
    [_tableView reloadData];
    [_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}


- (void)backPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - didReceiveMemoryWarning


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
