//
//  AbstractViewController.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LReachabilityController.h"

@interface AbstractViewController : UIViewController
{
    BOOL _visible;
}


@property (readonly, nonatomic, getter = isVisible) BOOL visible;


@end


#pragma mark - Protected methods


@interface AbstractViewController ()


- (void)initialize;
- (void)loadGUI;
- (void)bindGUI;
- (void)layoutGUI;
- (void)loadData;

- (void)appWillEnterForeground;
- (void)appDidEnterBackground;
- (void)appWillResignActive;
- (void)appDidBecomeActive;



@end
