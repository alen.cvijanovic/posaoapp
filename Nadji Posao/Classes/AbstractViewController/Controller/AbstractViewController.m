//
//  AbstractViewController.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "AbstractViewController.h"



@implementation AbstractViewController


#pragma mark - Synthesize


@synthesize visible = _visible;


#pragma mark - Init & Dealloc


- (id)init
{
    self = [super init];
    if (self)
    {
        [self initialize];
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];
    }
    return self;
}


- (void)initialize
{

}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Memory Warning


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Notification center


- (void)appWillEnterForeground
{
    
}


- (void)appDidEnterBackground
{
    
}


- (void)appWillResignActive
{
    
}


- (void)appDidBecomeActive
{
    
}


- (void)reachabilityChanged
{
    
}


#pragma mark - View


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadGUI];
    [self loadData];
    [self bindGUI];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _visible = YES;
    
    [self layoutGUI];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    _visible = NO;
}


#pragma mark loadGUI


- (void)loadGUI
{
    
}


#pragma mark bindGUI


- (void)bindGUI
{
    
}


#pragma mark layoutGUI


- (void)layoutGUI
{
    
}


#pragma mark - Data


- (void)loadData
{
    
}


#pragma mark - Orientation


- (BOOL)shouldAutorotate
{
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)loadReachability
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
    }
}


#pragma mark -


@end