//
//  MenuViewController.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "MenuViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "AboutViewController.h"
#import "SearchOptionsViewController.h"
#import "MRProgress.h"
#import "DataSource.h"
#import "SettingsViewController.h"
#import "FavoritesViewController.h"


@interface MenuViewController ()

@end

@implementation MenuViewController


#pragma mark - loadGUI


- (void)loadGUI
{
    [super loadGUI];
    self.view.backgroundColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchOptionsButtonPressed:)
                                                 name:@"OpenSearchOptions"
                                               object:nil];
}


#pragma mark -
#pragma mark Actions


- (IBAction)homeButtonPressed:(id)sender
{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}


- (IBAction)searchOptionsButtonPressed:(id)sender
{
    SearchOptionsViewController *searchOptionsViewController = [SearchOptionsViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchOptionsViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}


- (IBAction)favoritesButtonPressed:(id)sender
{
    FavoritesViewController *favoritesViewController = [FavoritesViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:favoritesViewController];
    [self presentViewController:navigationController animated:YES completion:nil];

}

- (IBAction)settingsButtonPressed:(id)sender
{
    SettingsViewController *settingsViewController = [SettingsViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (IBAction)aboutButtonPressed:(id)sender
{
    AboutViewController *aboutViewController = [AboutViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:aboutViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}


@end
