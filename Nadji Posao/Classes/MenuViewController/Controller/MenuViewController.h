//
//  MenuViewController.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"

@interface MenuViewController : AbstractViewController < UIAlertViewDelegate >
{
    __weak IBOutlet UILabel *_menuTitleLabel;
    __weak IBOutlet UIButton *_searchOptionsButton;
    __weak IBOutlet UIButton *_homeButton;
    __weak IBOutlet UIButton *_favoritesButton;
    __weak IBOutlet UIButton *_aboutButton;
}

@end
