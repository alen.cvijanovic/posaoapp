//
//  AboutViewController.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"

@interface AboutViewController : AbstractViewController < UIGestureRecognizerDelegate >
{
    __weak IBOutlet UIImageView *_logoImageView;
}

@end
