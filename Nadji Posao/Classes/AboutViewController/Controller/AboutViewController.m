//
//  AboutViewController.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "AboutViewController.h"
#import <MessageUI/MessageUI.h>

@interface AboutViewController ()< MFMailComposeViewControllerDelegate >

@end

@implementation AboutViewController

MFMailComposeViewController *_mailComposer;


#pragma mark - loadGUI


- (void)loadGUI
{
    [super loadGUI];
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Nazad" style:UIBarButtonItemStylePlain target:self action:@selector(backPressed:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logoTapped)];
    
    [_logoImageView addGestureRecognizer:tapGestureRecognizer];
}


#pragma mark - bindGUI


- (void)bindGUI
{
    self.title = @"O aplikaciji";
}


#pragma mark - MFMailComposeVC


- (IBAction)sendEmailButtonPressed:(id)sender
{
    _mailComposer = [[MFMailComposeViewController alloc] init];
    
    if ([MFMailComposeViewController canSendMail])
    {
        _mailComposer.mailComposeDelegate = self;
        [_mailComposer setSubject:nil];
        [_mailComposer setToRecipients:[NSArray arrayWithObjects:@"posaoapp@gmail.com", nil]];
        [self presentViewController:_mailComposer animated:YES completion:NULL];
    }
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
        {
            NSLog(@"Mail saved");
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Mail je uspješno poslat." message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            
            [alertView show];
            
            break;
        }
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
        {
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Greška prilikom slanja" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            
            [alertView show];
            
            break;
        }
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    _mailComposer = nil;
}


#pragma mark - Actions


- (void)logoTapped
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.hzz.hr"]];
}


- (void)backPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}





@end
