//
//  HomeViewController.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import <CBStoreHouseRefreshControl.h>
#import "MMDrawerController.h"

@interface HomeViewController : AbstractViewController < UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate >
{
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet NSLayoutConstraint *_topSpaceFromTableView;
    __weak IBOutlet UIButton *_scrollToTopButton;
    __weak IBOutlet UISegmentedControl *_segmentedControl;
    __weak IBOutlet UIButton *_sortButton;
    __weak IBOutlet UISearchBar *_searchBar;
    __weak IBOutlet UILabel *_noItemsLabel1;
    __weak IBOutlet UILabel *_noItemsLabel2;
    __weak IBOutlet UILabel *_noItemsLabel3;
    __weak IBOutlet UIView *_centerTextView;
}

@property (nonatomic, strong) CBStoreHouseRefreshControl *storeHouseRefreshControl;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) MMDrawerController *drawerController;

@end
