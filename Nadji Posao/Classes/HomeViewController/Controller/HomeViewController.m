//
//  HomeViewController.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "HomeViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "HomeTableViewCell.h"
#import "DataSource.h"
#import "County.h"
#import "DetailsViewController.h"
#import "MBProgressHUD.h"
#import "Parser.h"
#import <LReachabilityController.h>
#import "FavoritesTableViewCell.h"
#import "AppDelegate.h"


static NSString * const kSortByTitle = @"jobTitle";
static NSString * const kSortByPublishDate = @"jobStartingDate";
static NSString * const kSortBySeenDate = @"jobSeenDate";


@interface HomeViewController ()
{
    BOOL _optionsViewExpanded;
    NSArray *_dataArray;
    NSArray *_dataArrayCopy;
    CGFloat _maxCellHeight;
    BOOL _ascending;
    MBProgressHUD *_hud;
    UIView *_coverView;
    BOOL _search;
    BOOL _detailedSelected;
    BOOL _internetAvailable;
}

@end


@implementation HomeViewController


#pragma mark - loadGUI


- (void)loadGUI
{
    [super loadGUI];
    
    [self loadReachability];
    
    [self setupLeftMenuButton];
    self.edgesForExtendedLayout = UIRectEdgeNone;

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
    _tableView.backgroundColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    _centerTextView.backgroundColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];

    
    _tableView.alwaysBounceVertical = YES;
    
     self.storeHouseRefreshControl = [CBStoreHouseRefreshControl attachToScrollView:_tableView target:self refreshAction:@selector(refreshTriggered:) plist:@"AKTA" color:[UIColor blackColor] lineWidth:2 dropHeight:100 scale:0.6 horizontalRandomness:300 reverseLoadingAnimation:NO internalAnimationFactor:0.8];
    
    [_searchBar setReturnKeyType:UIReturnKeyDone];
    
    _ascending = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(currentlyParsedItemChanged)
                                                 name:@"CurrentlyParsed"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cacheDeleted)
                                                 name:@"CacheDeleted"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(historyDeleted)
                                                 name:@"HistoryDeleted"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(favoritesDeleted)
                                                 name:@"FavoritesDeleted"
                                               object:nil];

}


#pragma mark - loadData


- (void)loadData
{
    _detailedSelected = [[NSUserDefaults standardUserDefaults] boolForKey:@"DetailedSelected"];
    
    [[DataSource sharedDataSource] checkForExpiredJobs];
    
    if ([_segmentedControl selectedSegmentIndex] == 0)
    {
        _dataArray = [[DataSource sharedDataSource] getSelectedJobsWithSortingOption:kSortByTitle andAscending:_ascending andSearchString:_searchBar.text];
    }
    else if ([_segmentedControl selectedSegmentIndex] == 1)
    {
        _dataArray = [[DataSource sharedDataSource] getSelectedJobsWithSortingOption:kSortByPublishDate andAscending:!_ascending andSearchString:_searchBar.text];
    }
    else if ([_segmentedControl selectedSegmentIndex] == 2)
    {
        _dataArray = [[DataSource sharedDataSource] getSelectedJobsWithSortingOption:kSortBySeenDate andAscending:!_ascending andSearchString:_searchBar.text];
    }

    _dataArrayCopy = _dataArray;
    
    if ([_dataArray count] > 0)
    {
        _tableView.userInteractionEnabled = self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        _tableView.userInteractionEnabled = self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    
    _noItemsLabel1.hidden = _centerTextView.hidden = _noItemsLabel3.hidden = [_dataArray count];
}


#pragma mark - bindGUI


- (void)bindGUI
{
    self.title = @"Početna";
    
    [self layoutGUI];
}


#pragma mark - layoutGUI


- (void)layoutGUI
{
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


#pragma mark - viewDidAppear


- (void)viewDidAppear:(BOOL)animated
{
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"LastLoadingTime"]];
    
    if ([[LReachabilityController sharedReachabilityController] internetAvailable])
    {
        if ([[DataSource sharedDataSource] selectionChanged] || secondsBetween > 3600)
        {
            [[self mm_drawerController] closeDrawerAnimated:YES completion:^(BOOL finished) {
                [self finishRefreshControl];
                [[DataSource sharedDataSource] setSelectionChanged:NO];
                [_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                
            }];
        }
        
        [self loadData];
        
        [_tableView reloadData];
    }
}


#pragma mark - viewWillDisappear


- (void)viewWillDisappear:(BOOL)animated
{
    if (_optionsViewExpanded)
    {
        [self showHideOptionsView];
    }
}


#pragma mark - setupLeftMenuButton


- (void)setupLeftMenuButton
{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Search2"] style:UIBarButtonItemStylePlain target:self action:@selector(optionsPressed:)];
    self.navigationItem.rightBarButtonItem = rightButton;
}


#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_detailedSelected)
    {
        static NSString *favoriteCustomCellIdentifier = @"FavoriteViewCustomCell";
        
        FavoritesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:favoriteCustomCellIdentifier];
        
        if (cell == nil)
        {
            [tableView registerNib:[UINib nibWithNibName:@"FavoritesTableViewCell" bundle:nil] forCellReuseIdentifier:favoriteCustomCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:favoriteCustomCellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return cell;
    }
    else
    {
        static NSString *homeViewCustomCellIdentifier = @"homeViewCustomCell";
        
        HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:homeViewCustomCellIdentifier];
        
        if (cell == nil)
        {
            [tableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forCellReuseIdentifier:homeViewCustomCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:homeViewCustomCellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        return cell;
    }
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_detailedSelected)
    {
        FavoritesTableViewCell *myCell = (FavoritesTableViewCell *)cell;
        myCell.job = [_dataArray objectAtIndex:indexPath.row];
    }
    else
    {
        HomeTableViewCell *myCell = (HomeTableViewCell *)cell;
        myCell.job = [_dataArray objectAtIndex:indexPath.row];

    }
    
    if (indexPath.row == 40)
    {
        _scrollToTopButton .hidden = NO;
    }
    else if (indexPath.row < 40)
    {
        _scrollToTopButton.hidden = YES;
    }
    else if (indexPath.row > 40)
    {
        _scrollToTopButton.hidden = NO;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_detailedSelected)
    {
        return 141.0;
    }
    else
    {
        return 95.0;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[LReachabilityController sharedReachabilityController] internetAvailable] || [[[_dataArray objectAtIndex:indexPath.row] jobSeen] boolValue])
    {
        DetailsViewController *detailsViewController = [DetailsViewController new];
        detailsViewController.job = [_dataArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:detailsViewController animated:YES];
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Uređaj nije spojen na internet. Molim provjerite vašu internet konekciju." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alertView show];
    }
}

#pragma mark - Refresh

- (void)refreshTriggered:(id)sender
{
    int64_t delayInMiliseconds = 500;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInMiliseconds * NSEC_PER_MSEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self finishRefreshControl];
    });
}


- (void)finishRefreshControl
{
    if ([[LReachabilityController sharedReachabilityController] internetAvailable])
    {
        _hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
        
        _hud.labelText = @"Učitavanje...";
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [[DataSource sharedDataSource] performSelectedSync];
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [self.storeHouseRefreshControl finishingLoading];
                [self loadData];
                [_tableView reloadData];
            });
        });
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Uređaj nije spojen na internet. Molim provjerite vašu internet konekciju." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alertView show];
        [self.storeHouseRefreshControl finishingLoading];
    }
}


#pragma mark - Helpers


- (void)currentlyParsedItemChanged
{
    _hud.detailsLabelText = [[Parser sharedParser] currentlyParsedItem];
}


- (void)showHideOptionsView
{
    if (_optionsViewExpanded)
    {
        _topSpaceFromTableView.constant = _topSpaceFromTableView.constant - 122.0;
    }
    else
    {
        _topSpaceFromTableView.constant = _topSpaceFromTableView.constant + 122.0;
    }
    
    [_searchBar resignFirstResponder];
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                     } completion:^(BOOL finished) {
                     }];
    
    _optionsViewExpanded = !_optionsViewExpanded;
}


#pragma mark - Actions


- (void)leftDrawerButtonPress:(id)sender
{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}


- (void)optionsPressed:(UIBarButtonItem *)sender
{
    [self showHideOptionsView];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.storeHouseRefreshControl scrollViewDidScroll];
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.storeHouseRefreshControl scrollViewDidEndDragging];
}


- (IBAction)scrollToTopButtonPressed:(id)sender
{
    [_tableView setContentOffset:CGPointMake(0, 0 - _tableView.contentInset.top) animated:YES];
    _scrollToTopButton.hidden = YES;
}


- (IBAction)segmentedControlValueChanged:(id)sender
{
    [_tableView setContentOffset:CGPointMake(0, 0 - _tableView.contentInset.top) animated:YES];
    [self loadData];
    [_tableView reloadData];
}


- (IBAction)sortButtonPressed:(id)sender
{
    if (_ascending)
    {
        [_sortButton setImage:[UIImage imageNamed:@"generic_sortingDESC"] forState:UIControlStateNormal];
    }
    else
    {
        [_sortButton setImage:[UIImage imageNamed:@"generic_sortingASC"] forState:UIControlStateNormal];
    }
    _ascending = !_ascending;
    [self loadData];
    [_tableView reloadData];
    [_tableView setContentOffset:CGPointMake(0, 0 - _tableView.contentInset.top) animated:YES];
}


- (IBAction)searchOptionsButtonPressed:(id)sender
{
    [_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenSearchOptions" object:self];
    }];
}



#pragma mark - searchBar delegates


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    [self loadData];
    [_tableView reloadData];
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [_searchBar setShowsCancelButton:YES animated:YES];
    
    static UIButton *btnCancel;
    
    if (!btnCancel) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            UIView *topView = _searchBar.subviews[0];
            for (UIView *subView in topView.subviews) {
                if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                    btnCancel = (UIButton *)subView;
                }
            }
        } else {
            for (UIView *subView in _searchBar.subviews) {
                if ([subView isKindOfClass:NSClassFromString(@"UIButton")]) {
                    btnCancel = (UIButton *)subView;
                }
            }
        }
        if (btnCancel)
        {
            [btnCancel setTitle:@"Odustani" forState:UIControlStateNormal];
            [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCancel.titleLabel setAdjustsFontSizeToFitWidth:YES];
        }
    }
}


-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [_searchBar setShowsCancelButton:NO animated:YES];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self loadData];
    
    [_tableView reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
}


- (void)cacheDeleted
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Cache je uspješno obrisan." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alertView show];
}


- (void)historyDeleted
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Povijest gledanja je uspješno obrisana" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alertView show];
}


- (void)favoritesDeleted
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Favoriti su uspješno obrisani." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alertView show];
}


- (void)loadReachability
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        _internetAvailable = NO;
    } else {
        NSLog(@"There IS internet connection");
        _internetAvailable = YES;
    }
}


@end
