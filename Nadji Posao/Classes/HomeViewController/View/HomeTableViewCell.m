//
//  HomeTableViewCell.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "DataSource.h"

@implementation HomeTableViewCell

- (void)awakeFromNib
{
//    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
//    self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
    
    [_homeViewCellLabel setPreferredMaxLayoutWidth:[[UIScreen mainScreen] bounds].size.width - 20];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (void)setJob:(Job *)job
{
    _homeViewCellLabel.text = job.jobTitle;
    _seenImageView.hidden = YES;
    
    if ([job toFavorite])
    {
       _seenImageView.hidden = NO;
        [_seenImageView setImage:[UIImage imageNamed:@"favorite_Filled"]];
    }
    else if ([job toHistory])
    {
        _seenImageView.hidden = NO;
        [_seenImageView setImage:[UIImage imageNamed:@"seen"]];
    }
}


- (void)setShowSeenImageView:(BOOL)showSeenImageView
{
    _seenImageView.hidden = !showSeenImageView;
}

@end
