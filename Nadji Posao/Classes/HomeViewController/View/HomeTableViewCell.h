//
//  HomeTableViewCell.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Job.h"

@interface HomeTableViewCell : UITableViewCell
{
    __weak IBOutlet UILabel *_homeViewCellLabel;
    __weak IBOutlet UIImageView *_seenImageView;
    __weak IBOutlet UILabel *_titleLabel;
    
}

@property (nonatomic) BOOL showSeenImageView;
@property (nonatomic, strong) Job *job;

@end
