//
//  SearchOptionsViewController.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"

@interface SearchOptionsViewController : AbstractViewController < UITableViewDataSource, UITableViewDelegate >
{
    __weak IBOutlet UIButton *_selectCountyButton;
    __weak IBOutlet UIButton *_selectCategoryButton;
    __weak IBOutlet UITableView *_tableView;
}

@end
