//
//  SearchOptionsViewController.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "SearchOptionsViewController.h"
#import "SearchOptionsTableViewCell.h"
#import "DataSource.h"

@interface SearchOptionsViewController ()
{
    NSArray *_dataArray;
    BOOL _categoriesSelected;
}

@end

@implementation SearchOptionsViewController


#pragma mark - loadGUI


- (void)loadGUI
{
    [super loadGUI];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Nazad" style:UIBarButtonItemStylePlain target:self action:@selector(backPressed:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [_tableView setAllowsSelection:YES];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
    _tableView.backgroundColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    
    [_selectCountyButton setBackgroundColor:[UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [_selectCategoryButton setBackgroundColor:[UIColor blackColor]];
}


#pragma mark - loadData


- (void)loadData
{
    if (_categoriesSelected)
    {
        _dataArray = [[DataSource sharedDataSource] allCategories];
    }
    else
    {
        _dataArray = [[DataSource sharedDataSource] allCounties];
    }
}


#pragma mark - bindGUI


- (void)bindGUI
{
    self.title = @"Opcije prikaza";
}


#pragma mark - layoutGUI


- (void)layoutGUI
{
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *searchOptionsCustomCellIdentifier = @"searchOptionsTableViewCell";
    
    SearchOptionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:searchOptionsCustomCellIdentifier];
    
    if (cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"SearchOptionsTableViewCell" bundle:nil] forCellReuseIdentifier:searchOptionsCustomCellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:searchOptionsCustomCellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    if (_categoriesSelected)
    {
        cell.searchOptionsCellLabel.text = [[_dataArray objectAtIndex:indexPath.row] categoryTitle];
        
        if ([[[_dataArray objectAtIndex:indexPath.row] categorySelected] boolValue])
        {
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
    else
    {
        cell.searchOptionsCellLabel.text = [[_dataArray objectAtIndex:indexPath.row] countyTitle];
        
        if ([[[_dataArray objectAtIndex:indexPath.row] countySelected] boolValue])
        {
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(SearchOptionsTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_categoriesSelected)
    {
        cell.searchOptionsCellLabel.text = [[_dataArray objectAtIndex:indexPath.row] categoryTitle];
        
        if ([[[_dataArray objectAtIndex:indexPath.row] categorySelected] boolValue])
        {
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
    else
    {
        cell.searchOptionsCellLabel.text = [[_dataArray objectAtIndex:indexPath.row] countyTitle];
        
        if ([[[_dataArray objectAtIndex:indexPath.row] countySelected] boolValue])
        {
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_categoriesSelected)
    {
        [[_dataArray objectAtIndex:indexPath.row] setCategorySelected:@1];
        [[DataSource sharedDataSource] updateJobCategory:[_dataArray objectAtIndex:indexPath.row]];
    }
    else
    {
        [[_dataArray objectAtIndex:indexPath.row] setCountySelected:@1];
        [[DataSource sharedDataSource] updateCounty:[_dataArray objectAtIndex:indexPath.row]];
    }
    
    [[DataSource sharedDataSource] setSelectionChanged:YES];
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (_categoriesSelected)
    {
        [[_dataArray objectAtIndex:indexPath.row] setCategorySelected:@0];
        [[DataSource sharedDataSource] updateJobCategory:[_dataArray objectAtIndex:indexPath.row]];
    }
    else
    {
        [[_dataArray objectAtIndex:indexPath.row] setCountySelected:@0];
        [[DataSource sharedDataSource] updateCounty:[_dataArray objectAtIndex:indexPath.row]];
    }
    
    [[DataSource sharedDataSource] setSelectionChanged:YES];
}


#pragma mark - Actions


- (IBAction)countiesButtonSelected:(id)sender
{
    if (_categoriesSelected)
    {
        [_selectCountyButton setBackgroundColor:[UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0]];
        [_selectCategoryButton setBackgroundColor:[UIColor blackColor]];
        
        _categoriesSelected = NO;
        
        [self loadData];
        [_tableView reloadData];
        
        [_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
}


- (IBAction)jobCategoriesButtonSelected:(id)sender
{
    if (!_categoriesSelected)
    {
        [_selectCountyButton setBackgroundColor:[UIColor blackColor]];
        [_selectCategoryButton setBackgroundColor:[UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0]];
        
        
        _categoriesSelected = YES;
        
        [self loadData];
        [_tableView reloadData];
        
        [_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
}


- (void)backPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
