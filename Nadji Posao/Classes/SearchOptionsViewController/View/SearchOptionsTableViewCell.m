//
//  SearchOptionsTableViewCell.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "SearchOptionsTableViewCell.h"
#import "DataSource.h"

@implementation SearchOptionsTableViewCell

- (void)awakeFromNib
{
    // Initialization code

//    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
//    self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    if (selected)
    {
        self.contentView.backgroundColor = [UIColor blackColor];
        [self.searchOptionsCellLabel setTextColor:[UIColor whiteColor]];
        _checkmarkImageView.hidden = NO;
    }
    else
    {
        self.contentView.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
        [self.searchOptionsCellLabel setTextColor:[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]];
        _checkmarkImageView.hidden = YES;
    }
}



@end
