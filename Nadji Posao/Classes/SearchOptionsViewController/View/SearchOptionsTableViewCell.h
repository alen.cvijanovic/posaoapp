//
//  SearchOptionsTableViewCell.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchOptionsTableViewCell : UITableViewCell < UIGestureRecognizerDelegate >

@property (weak, nonatomic) IBOutlet UILabel *searchOptionsCellLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkmarkImageView;


@end
