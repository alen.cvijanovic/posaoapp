//
//  SettingsViewController.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 12/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"

@interface SettingsViewController : AbstractViewController < UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate >
{
    __weak IBOutlet UITableView *_tableView;
}

@end
