//
//  SettingsViewController.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 12/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsTableViewCell.h"
#import "DataSource.h"
#import <Parse/Parse.h>

@interface SettingsViewController ()
{
    NSArray *_dataArray;
}

@end


@implementation SettingsViewController


#pragma mark - loadGUI


- (void)loadGUI
{
    [super loadGUI];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
    _tableView.backgroundColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Nazad" style:UIBarButtonItemStylePlain target:self action:@selector(backPressed:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.title = @"Postavke";
    
    [PFAnalytics trackEvent:@"Favorites opened"];
}


#pragma mark - loadData


- (void)loadData
{
    _dataArray = [[NSArray alloc] initWithObjects:@"Obriši povijest gledanja", @"Obriši favorite", @"Obriši cache", nil];
}


#pragma mark - bindGUI


- (void)bindGUI
{
    [self layoutGUI];
}


#pragma mark - layoutGUI


- (void)layoutGUI
{
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [_dataArray count];
    }
    else
    {
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *settingsCustomCellIdentifier = @"SettingsCustomCell";
    
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsCustomCellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"SettingsTableViewCell" bundle:nil] forCellReuseIdentifier:settingsCustomCellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:settingsCustomCellIdentifier];
    }
    
    if (indexPath.section == 0)
    {
        cell.title = [_dataArray objectAtIndex:indexPath.row];
        cell.switcher.hidden = YES;
    }
    else
    {
        if (indexPath.row == 0)
        {
            cell.title = @"Detaljniji prikaz oglasa";
            cell.switcher.hidden = NO;
            cell.tag = 202;
            
            cell.switcher.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"DetailedSelected"];
        }
        else if (indexPath.row == 1)
        {
            cell.title = @"Tamna tema prikaza";
            cell.switcher.hidden = NO;
            cell.tag = 303;
            
            cell.switcher.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"DarkThemeSelected"];
        }
        
    }
    
   
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,30)];

    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor blackColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:18];
    headerLabel.frame = CGRectMake(0,0,self.view.frame.size.width,20);
    
    if (section == 0)
    {
        headerLabel.text =  @"Brisanje podataka iz memorije uređaja";
    }
    else
    {
       headerLabel.text =  @"Postavke prikaza";
    }

    headerLabel.textColor = [UIColor lightTextColor];
    [headerLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0]];
    
    [containerView addSubview:headerLabel];
    
    
    return containerView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pozor!" message:@"Ovom radnjom ćete obrisati čitavu povijest gledanja!\nŽelite li nastaviti?" delegate:self cancelButtonTitle:@"Ne" otherButtonTitles:@"Da", nil];
            
            alert.tag = 1;
            
            [alert show];
        }
        else if (indexPath.row == 1)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pozor!" message:@"Ovom radnjom ćete obrisati sve favorite!\nŽelite li nastaviti?" delegate:self cancelButtonTitle:@"Ne" otherButtonTitles:@"Da", nil];
            
            alert.tag = 2;
            
            [alert show];
        }
        else if (indexPath.row == 2)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pozor!" message:@"Ovom radnjom ćete obrisati sve spremljene podatke iz aplikacije!\nŽelite li nastaviti?" delegate:self cancelButtonTitle:@"Ne" otherButtonTitles:@"Da", nil];
            
            alert.tag = 3;
            
            [alert show];
        }
    }
    else if (indexPath.section == 1)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Postavljanjem detaljnijeg prikaza, na početnom zaslonu ćete vidjeti više podataka." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        alert.tag = 4;
        
        [alert show];

    }
}



#pragma mark - alertView


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (alertView.tag == 1)
        {
            [[DataSource sharedDataSource] deleteHistory];
        }
        else if (alertView.tag == 2)
        {
            [[DataSource sharedDataSource] deleteFavorites];
        }
        else if (alertView.tag == 3)
        {
            [[DataSource sharedDataSource] deleteCache];
        }
    }
}






#pragma mark - Actions


- (void)backPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - didReceiveMemoryWarning


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
