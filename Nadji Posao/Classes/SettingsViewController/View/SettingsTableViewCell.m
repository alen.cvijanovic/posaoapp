//
//  SettingsTableViewCell.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 16/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "SettingsTableViewCell.h"

@implementation SettingsTableViewCell

- (void)awakeFromNib
{
    [_switcher addTarget:self action:@selector(stateChanged:) forControlEvents:UIControlEventValueChanged];
//    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
//    self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"outer"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setTitle:(NSString *)title
{
    _title = title;
    _titleLabel.text = title;
}


- (void)stateChanged:(UISwitch *)switcher
{
    if (self.tag == 202)
    {
        [[NSUserDefaults standardUserDefaults] setBool:switcher.on forKey:@"DetailedSelected"];
    }
//    else if (self.tag == 303)
//    {
//        [[NSUserDefaults standardUserDefaults] setBool:switcher.on forKey:@"DarkThemeSelected"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"ThemeChanged" object:self];
//    }
    
}


@end
