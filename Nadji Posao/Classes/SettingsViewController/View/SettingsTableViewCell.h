//
//  SettingsTableViewCell.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 16/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell
{
    __weak IBOutlet UILabel *_titleLabel;
}

@property (nonatomic, strong) NSString *title;
@property (weak, nonatomic) IBOutlet UISwitch *switcher;

@end
