//
//  Parser.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "Job.h"

@interface Parser : NSObject < NSXMLParserDelegate, NSFetchedResultsControllerDelegate >


@property (nonatomic, strong) NSMutableDictionary *currentItem;   // current section being parsed
@property (nonatomic, strong) NSMutableDictionary *xmlData;          // completed parsed xml response
@property (nonatomic, strong) NSString *elementName;
@property (nonatomic, strong) NSMutableString *currentString;
@property (nonatomic, strong) NSXMLParser *nsxmlParser;

@property (nonatomic, strong) Job *job;
@property (nonatomic, strong) Job *currentJob;

@property (nonatomic, strong) NSString *currentlyParsedItem;


+ (id)sharedParser;

- (void)parseDataForCounty:(County *)county;
- (void)parseDataForJobCategory:(JobCategory *)jobCategory;


@end
