//
//  DataSource.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataController.h"
#import "County.h"
#import "Job.h"
#import "JobCategory.h"

@interface DataSource : NSObject < NSFetchedResultsControllerDelegate >

@property (nonatomic) BOOL firstLoad;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) County *county;
@property (nonatomic, strong) Job *job;
@property (nonatomic, strong) JobCategory *jobCategory;
@property (nonatomic, strong) NSArray *allCounties;
@property (nonatomic, strong) NSArray *allCategories;
@property (nonatomic) BOOL selectionChanged;
@property (nonatomic, strong) NSDate *dateOfLastLoading;

+ (id)sharedDataSource;

- (NSManagedObjectContext *)managedObjectContext;

- (void)loadMainData;

- (NSArray *)getCounties;
- (NSArray *)getJobCategories;
- (NSArray *)getJobs;
- (NSArray *)getFavorites;

- (void)updateCounty:(County *)county;
- (void)updateJobCategory:(JobCategory *)jobCategory;

- (void)checkForExpiredJobs;

- (NSArray *)getJobsForSelectedCounties;
- (NSArray *)getJobsForSelectedCategories;
- (NSArray *)getHistoryJobs;

- (NSArray *)getSelectedJobs;
- (NSArray *)getSelectedCounties;
- (NSArray *)getSelectedJobCategories;
- (NSArray *)getSelectedJobsWithSortingOption:(NSString *)option andAscending:(BOOL)ascending andSearchString:(NSString *)string;

- (void)addFavoriteToJob:(Job *)job;
- (void)removeFavoriteJob:(Job *)job;

- (void)performFullSync;
- (void)performSelectedSync;

- (void)jobSeen:(Job *)job;

- (void)deleteHistory;
- (void)deleteFavorites;
- (void)deleteCache;
- (void)deleteAllJobs;
- (void)deleteJob:(Job *)job;


@end
