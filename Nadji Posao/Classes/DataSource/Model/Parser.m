//
//  Parser.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "Parser.h"
#import "DataSource.h"
#import "CoreDataController.h"

@implementation Parser
{
    County *_currentlyParsedCounty;
    JobCategory *_currentlyParsedJobCategory;
    BOOL _item;
}


#pragma mark - Singleton


+ (id)sharedParser
{
    static Parser *sharedParser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedParser = [[self alloc] init];
    });
    return sharedParser;
}


- (id)init
{
    if (self = [super init])
    {
    }
    return self;
}


#pragma mark - Actions


- (void)parseDataForCounty:(County *)county
{
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:county.countyLoadedTime];
    
    if (county.countyLoadedTime == nil || secondsBetween > 3600)
    {
        _currentlyParsedCounty = county;
        
        _currentlyParsedItem = county.countyTitle;
        
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"CurrentlyParsed" object:nil]];
        
        NSString *string = county.countyURL;
        NSURL *url = [NSURL URLWithString:string];
        
        _nsxmlParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        [_nsxmlParser setShouldProcessNamespaces:YES];
        
        _nsxmlParser.delegate = self;
        [_nsxmlParser parse];
    }
    _currentlyParsedCounty = nil;
}


- (void)parseDataForJobCategory:(JobCategory *)jobCategory
{
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:jobCategory.categoryLoadedTime];
    
    if (jobCategory.categoryLoadedTime == nil || secondsBetween > 3600)
    {
        _currentlyParsedJobCategory = jobCategory;
        
        _currentlyParsedItem = jobCategory.categoryTitle;
        
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"CurrentlyParsed" object:nil]];
        
        NSString *string = jobCategory.categoryURL;
        NSURL *url = [NSURL URLWithString:string];
        
        _nsxmlParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        [_nsxmlParser setShouldProcessNamespaces:YES];
        
        _nsxmlParser.delegate = self;
        [_nsxmlParser parse];
    }
    
    _currentlyParsedJobCategory = nil;
}


#pragma mark - Parser


- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    self.xmlData = [NSMutableDictionary dictionary];
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    self.elementName = qName;
    
    if([qName isEqualToString:@"item"])
    {
        _currentItem = nil;
        _currentItem = [[NSMutableDictionary alloc] init];
    }
    else if([qName isEqualToString:@"title"] || [qName isEqualToString:@"pubDate"] || [qName isEqualToString:@"link"] || [qName isEqualToString:@"description"])
    {
        _currentString = nil;
        self.currentString = [[NSMutableString alloc] init];
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (!self.elementName)
        return;
    
    if([self.elementName isEqualToString:@"title"])
    {
        [self.currentString appendFormat:@"%@", string];
    }
    else if([self.elementName isEqualToString:@"pubDate"])
    {
        [self.currentString appendFormat:@"%@", string];
    }
    else if([self.elementName isEqualToString:@"link"])
    {
        [self.currentString appendFormat:@"%@", string];
    }
    else if([self.elementName isEqualToString:@"description"])
    {
        [self.currentString appendFormat:@"%@", string];
    }
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Job"];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"jobID == %@", [_currentItem objectForKey:@"jobID"]];
    [fetch setPredicate:predicate];

    NSError *error = nil;

    NSArray *jobObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    if ([qName isEqualToString:@"item"])
    {
        _item = YES;
        
        if([jobObjects count] == 0)
        {
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Job" inManagedObjectContext:[self managedObjectContext]];
            _job = [[Job alloc]initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
            _job.jobTitle = [_currentItem objectForKey:@"jobTitle"];
            _job.jobStartingDate = [_currentItem objectForKey:@"pubDate"];
            _job.jobURL = [_currentItem objectForKey:@"link"];
            _job.jobID = [_currentItem objectForKey:@"jobID"];
            _job.toCounty = _currentlyParsedCounty;
            _job.toJobCategory = _currentlyParsedJobCategory;
            _job.jobEndDate = [self dateFromString:[_currentItem objectForKey:@"endDate"]];
            _job.jobLocation = [_currentItem objectForKey:@"jobLocation"];
            
            _job.jobLocationKeyword = [_job.jobLocation stringByReplacingOccurrencesOfString:@"Č" withString:@"C"];
            _job.jobLocationKeyword = [_job.jobLocationKeyword stringByReplacingOccurrencesOfString:@"Ć" withString:@"C"];
            _job.jobLocationKeyword = [_job.jobLocationKeyword stringByReplacingOccurrencesOfString:@"Š" withString:@"S"];
            _job.jobLocationKeyword = [_job.jobLocationKeyword stringByReplacingOccurrencesOfString:@"Ž" withString:@"Z"];
            _job.jobLocationKeyword = [_job.jobLocationKeyword stringByReplacingOccurrencesOfString:@"Đ" withString:@"D"];
            
            _job.jobKeyword = [_job.jobTitle stringByReplacingOccurrencesOfString:@"Č" withString:@"C"];
            _job.jobKeyword = [_job.jobKeyword stringByReplacingOccurrencesOfString:@"Ć" withString:@"C"];
            _job.jobKeyword = [_job.jobKeyword stringByReplacingOccurrencesOfString:@"Š" withString:@"S"];
            _job.jobKeyword = [_job.jobKeyword stringByReplacingOccurrencesOfString:@"Ž" withString:@"Z"];
            _job.jobKeyword = [_job.jobKeyword stringByReplacingOccurrencesOfString:@"Đ" withString:@"D"];
            
            [[self managedObjectContext] save:&error];
        }
        else
        {
            if (_currentlyParsedCounty == nil)
            {
                [[jobObjects firstObject] setToJobCategory:_currentlyParsedJobCategory];
            }
            else if (_currentlyParsedJobCategory == nil)
            {
                [[jobObjects firstObject] setToCounty:_currentlyParsedCounty];
            }
        }
        _job = nil;
    }
    else if ([qName isEqualToString:@"title"])
    {
        NSString *string = _currentString;
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        [_currentItem setObject:string forKey:@"jobTitle"];
    }
    else if ([qName isEqualToString:@"pubDate"])
    {
        [_currentItem setObject:[self dateFromString:_currentString] forKey:@"pubDate"];
    }
    else if ([qName isEqualToString:@"guid"])
    {
        NSString *string = _currentString;
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        [_currentItem setObject:string forKey:@"link"];
        NSString *input = _currentString;
        NSArray *parts = [input componentsSeparatedByString:@"="];
        [_currentItem setObject:parts[1] forKey:@"jobID"];
    }
    else if ([qName isEqualToString:@"description"] && _item)
    {
        NSString *stringForLocation = _currentString;
        NSRange range1 = [stringForLocation rangeOfString:@"Mjesto rada: "];
        NSRange range2 = [stringForLocation rangeOfString:@", Općina"];
        
        if (range1.location != NSNotFound && range2.location != NSNotFound)
        {
            NSString *locationString = [stringForLocation substringWithRange:NSMakeRange(range1.location+13, range2.location - (range1.location+13))];
            [_currentItem setObject:locationString forKey:@"jobLocation"];
        }
        
        NSRange range3 = [stringForLocation rangeOfString:@"Rok za prijavu: "];
        NSRange range4 = [stringForLocation rangeOfString:@", Mjesto rada"];
        
        if (range1.location != NSNotFound && range2.location != NSNotFound)
        {
            NSString *endDateString = [stringForLocation substringWithRange:NSMakeRange(range3.location+16, range4.location - (range3.location+16))];
            [_currentItem setObject:endDateString forKey:@"endDate"];
        }
    }
}


- (void) parserDidEndDocument:(NSXMLParser *)parser
{
    NSLog(@"parserDidEndDocument");
    
    if (_currentlyParsedCounty)
    {
        _currentlyParsedCounty.countyLoadedTime = [NSDate date];
        [[DataSource sharedDataSource] updateCounty:_currentlyParsedCounty];
    }
    else
    {
        _currentlyParsedJobCategory.categoryLoadedTime = [NSDate date];
        [[DataSource sharedDataSource] updateJobCategory:_currentlyParsedJobCategory];
    }
    
    _item = NO;
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"LastLoadingTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Helpers


- (NSManagedObjectContext *)managedObjectContext
{
    return [[CoreDataController sharedCDController] backgroundObjectContext];
}



- (NSDate *)dateFromString:(NSString *)string
{
    NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    return [dateFormatter dateFromString:string];
}

@end
