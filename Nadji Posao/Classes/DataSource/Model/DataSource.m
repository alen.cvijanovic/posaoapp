//
//  DataSource.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "DataSource.h"
#import "CoreDataController.h"
#import "County.h"
#import "JobCategory.h"
#import "Job.h"
#import "Favorite.h"
#import "Parser.h"
#import "MBProgressHUD.h"
#import "NSDate+Utilities.h"
#import "History.h"


@implementation DataSource

#pragma mark - Singleton


+ (id)sharedDataSource
{
    static DataSource *sharedDataSource = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDataSource = [[self alloc] init];
    });
    return sharedDataSource;
}

- (id)init
{
    if (self = [super init])
    {
        [self loadMainData];
    }
    return self;
}


#pragma mark - Initialization


- (void)loadMainData
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FirstLoadDone"])
    {
        [self initCounties];
        [self initJobCategories];
        [self initFavorites];
        NSLog(@"Main Data Imported");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FirstLoadDone"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    _allCounties = [NSArray arrayWithArray:[self getCounties]];
    _allCategories = [NSArray arrayWithArray:[self getJobCategories]];
    
    NSLog(@"Main Data Loaded");
}


- (void)initCounties
{
    NSArray *countyNames = [[NSArray alloc] initWithObjects:@"Bjelovarsko-bilogorska", @"Brodsko-posavska", @"Dubrovačko-neretvanska", @"Grad Zagreb", @"Istarska", @"Karlovačka", @"Koprivničko-Križevačka", @"Krapinsko-zagorska", @"Ličko-senjska", @"Međimurska", @"Osječko-baranjska", @"Požeško-slavonska", @"Primorsko-goranska", @"Sisačko-moslavačka", @"Splitsko-dalmatinska", @"Šibensko-kninska", @"Varaždinska", @"Virovitičko-podravska", @"Vukovarsko-srijemska", @"Zadarska", @"Zagrebačka", @"Poslovi u EU", nil];
    
    
    NSArray *countyURLs = [[NSArray alloc] initWithObjects:@"http://burzarada.hzz.hr/rss/rsszup1.xml", @"http://burzarada.hzz.hr/rss/rsszup2.xml", @"http://burzarada.hzz.hr/rss/rsszup3.xml", @"http://burzarada.hzz.hr/rss/rsszup4.xml", @"http://burzarada.hzz.hr/rss/rsszup5.xml", @"http://burzarada.hzz.hr/rss/rsszup6.xml", @"http://burzarada.hzz.hr/rss/rsszup7.xml", @"http://burzarada.hzz.hr/rss/rsszup8.xml", @"http://burzarada.hzz.hr/rss/rsszup9.xml", @"http://burzarada.hzz.hr/rss/rsszup10.xml", @"http://burzarada.hzz.hr/rss/rsszup11.xml", @"http://burzarada.hzz.hr/rss/rsszup12.xml", @"http://burzarada.hzz.hr/rss/rsszup13.xml", @"http://burzarada.hzz.hr/rss/rsszup14.xml", @"http://burzarada.hzz.hr/rss/rsszup15.xml", @"http://burzarada.hzz.hr/rss/rsszup16.xml", @"http://burzarada.hzz.hr/rss/rsszup17.xml", @"http://burzarada.hzz.hr/rss/rsszup18.xml", @"http://burzarada.hzz.hr/rss/rsszup19.xml", @"http://burzarada.hzz.hr/rss/rsszup20.xml", @"http://burzarada.hzz.hr/rss/rsszup21.xml", @"http://burzarada.hzz.hr/rss/rsszup1000.xml", nil];
    
    
    for (int i = 0; i<[countyNames count]; i++)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"County" inManagedObjectContext:[self managedObjectContext]];
        County *county = [[County alloc]initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
        
        county.countyTitle = [countyNames objectAtIndex:i];
        county.countyURL = [countyURLs objectAtIndex:i];
        county.countyID = [NSString stringWithFormat:@"%d", i];
        
        NSError *error = nil;
        if ([[self managedObjectContext] hasChanges])
        {
            if (![[self managedObjectContext] save:&error])
            {
                NSLog(@"Save Failed: %@", [error localizedDescription]);
            } else {
                NSLog(@"Save succeeded %@", [countyNames objectAtIndex:i]);
            }
        }
        county = nil;
    }
}


- (void)initJobCategories
{
    NSArray *jobCategoryTitles = [[NSArray alloc] initWithObjects:@"Direktori, menadžeri i čelnici", @"Stručnjaci iz područja informatike i tehničkih znanosti", @"Zdravstveni, prirodoslovni i biotehnički stručnjaci", @"Profesori, nastavnici i stručnjaci za obrazovanje", @"Stručnjaci iz društvenog, humanističkog i umjetničkog područja", @"Uredski i šalterski službenici", @"Uslužna i ugostiteljska zanimanja", @"Trgovačka i srodna zanimanja", @"Kvalificirani poljoprivredni, ribarski i šumarski radnici", @"Građevinska i rudarska zanimanja", @"Mehaničari, strojarski monteri, elektromonteri i obrađivači metala", @"Tiskari, precizni mehaničari, keramičari i staklari", @"Zanimanja u proizvodnji i preradi hrane, tekstila, kože i drva", @"Rukovatelji strojevima i postrojenjima, sastavljači proizvoda", @"Vozači motornih vozila, pokretnih strojeva i brodska posada", @"Jednostavna prodajna i uslužna zanimanja ", @"Jednostavna poljoprivredna, šumarska i ribarska zanimanja", @"Jednostavna građevinska, proizvodna i transportna zanimanja", nil];
    
    NSArray *jobCategoryURLs = [[NSArray alloc] initWithObjects:@"http://burzarada.hzz.hr/rss/rsskat1001.xml", @"http://burzarada.hzz.hr/rss/rsskat1002.xml", @"http://burzarada.hzz.hr/rss/rsskat1003.xml", @"http://burzarada.hzz.hr/rss/rsskat1004.xml", @"http://burzarada.hzz.hr/rss/rsskat1005.xml", @"http://burzarada.hzz.hr/rss/rsskat1011.xml", @"http://burzarada.hzz.hr/rss/rsskat1012.xml", @"http://burzarada.hzz.hr/rss/rsskat1013.xml", @"http://burzarada.hzz.hr/rss/rsskat1014.xml", @"http://burzarada.hzz.hr/rss/rsskat1015.xml", @"http://burzarada.hzz.hr/rss/rsskat1016.xml", @"http://burzarada.hzz.hr/rss/rsskat1017.xml", @"http://burzarada.hzz.hr/rss/rsskat1018.xml", @"http://burzarada.hzz.hr/rss/rsskat1020.xml", @"http://burzarada.hzz.hr/rss/rsskat1021.xml", @"http://burzarada.hzz.hr/rss/rsskat1022.xml", @"http://burzarada.hzz.hr/rss/rsskat1023.xml", @"http://burzarada.hzz.hr/rss/rsskat1024.xml", nil];
    
    for (int i = 0; i<[jobCategoryTitles count]; i++)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"JobCategory" inManagedObjectContext:[self managedObjectContext]];
        JobCategory *jobCategory = [[JobCategory alloc]initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];

        jobCategory.categoryTitle = [jobCategoryTitles objectAtIndex:i];
        jobCategory.categoryURL = [jobCategoryURLs objectAtIndex:i];
        jobCategory.categoryID = [NSString stringWithFormat:@"%d", i];
        
        NSError *error = nil;
        if ([self.managedObjectContext hasChanges])
        {
            if (![self.managedObjectContext save:&error])
            {
                NSLog(@"Save Failed: %@", [error localizedDescription]);
            } else {
                NSLog(@"Save succeeded %@", [jobCategoryTitles objectAtIndex:i]);
            }
        }
        jobCategory = nil;
    }
}


- (void)initFavorites
{
    NSEntityDescription *entityFavorite = [NSEntityDescription entityForName:@"Favorite" inManagedObjectContext:[self managedObjectContext]];
   __unused Favorite *favorite = [[Favorite alloc]initWithEntity:entityFavorite insertIntoManagedObjectContext:[self managedObjectContext]];
    NSEntityDescription *entityHistory = [NSEntityDescription entityForName:@"History" inManagedObjectContext:[self managedObjectContext]];
    __unused History *history = [[History alloc]initWithEntity:entityHistory insertIntoManagedObjectContext:[self managedObjectContext]];
    
    NSError *error = nil;
    if ([self.managedObjectContext hasChanges])
    {
        if (![self.managedObjectContext save:&error])
        {
            NSLog(@"Save Failed: %@", [error localizedDescription]);
        } else {

        }
    }
}


#pragma mark - Fetched Results Controller Section


- (NSFetchedResultsController *)fetchedResultsController:(NSString *)type
{
    if (_fetchedResultsController != nil)
    {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    
    if ([type isEqualToString:@"County"])
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"County" inManagedObjectContext:context];
        
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"countyTitle" ascending:YES];
        
        NSArray *sortDescriptors = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
        
        fetchRequest.sortDescriptors = sortDescriptors;
    }
    else if ([type isEqualToString:@"JobCategory"])
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"JobCategory" inManagedObjectContext:context];
        
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryTitle" ascending:YES];
        
        NSArray *sortDescriptors = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
        
        fetchRequest.sortDescriptors = sortDescriptors;
    }
    else if ([type isEqualToString:@"Job"])
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Job" inManagedObjectContext:context];
        
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"jobTitle" ascending:YES];
        
        NSArray *sortDescriptors = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
        
        fetchRequest.sortDescriptors = sortDescriptors;
    }
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}


#pragma mark - Helpers


- (NSManagedObjectContext *)managedObjectContext
{
    return [[CoreDataController sharedCDController] backgroundObjectContext];
}


- (NSArray *)getCounties
{
    NSError *error = nil;
    NSFetchedResultsController *fetchedResultsController = [self fetchedResultsController:@"County"];
    if (![fetchedResultsController performFetch:&error])
    {
        NSLog(@"Error! %@", error);
    }
    
    NSArray *array = [fetchedResultsController fetchedObjects];

    _fetchedResultsController = nil;
    
    return array;
}


- (NSArray *)getSelectedCounties
{
    NSMutableArray *selectedCounties = [[NSMutableArray alloc] init];
    
    for (County *county in [self getCounties])
    {
        if ([[county countySelected] boolValue])
        {
            [selectedCounties addObject:county];
        }
    }
    
    return selectedCounties;
}


- (NSArray *)getJobCategories
{
    NSError *error = nil;
    NSFetchedResultsController *fetchedResultsController = [self fetchedResultsController:@"JobCategory"];
    if (![fetchedResultsController performFetch:&error])
    {
        NSLog(@"Error! %@", error);
    }
    
    NSArray *array = [fetchedResultsController fetchedObjects];
    
    _fetchedResultsController = nil;
    
    return array;
}


- (NSArray *)getSelectedJobCategories
{
    NSMutableArray *selectedJobCategories = [[NSMutableArray alloc] init];
    
    for (JobCategory *jobCategory in [self getJobCategories])
    {
        if ([[jobCategory categorySelected] boolValue])
        {
            [selectedJobCategories addObject:jobCategory];
        }
    }
    
    return selectedJobCategories;
}


- (NSArray *)getJobs
{
    NSError *error = nil;
    NSFetchedResultsController *fetchedResultsController = [self fetchedResultsController:@"Job"];
    if (![fetchedResultsController performFetch:&error])
    {
        NSLog(@"Error! %@", error);
    }
    
    NSArray *array = [fetchedResultsController fetchedObjects];
    
    _fetchedResultsController = nil;
    
    return array;
}


- (void)updateCounty:(County *)county
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"County"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"countyID == %@", county.countyID];
    [fetch setPredicate:predicate];
    
    NSError *error = nil;
    
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    if ([fetchedObjects count] == 1)
    {
        County *countyForUpdate = [fetchedObjects firstObject];
        countyForUpdate = county;
        
        NSError *error = nil;
        if ([[self managedObjectContext] hasChanges]) {
            if (![[self managedObjectContext] save:&error]) {
                NSLog(@"Save Failed: %@", [error localizedDescription]);
            } else {
                NSLog(@"Save succeeded");
            }
        };
    }
}


- (void)updateJobCategory:(JobCategory *)jobCategory
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"JobCategory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categoryID == %@", jobCategory.categoryID];
    [fetch setPredicate:predicate];
    
    NSError *error = nil;
    
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    if ([fetchedObjects count] == 1)
    {
        JobCategory *jobCategoryForUpdate = [fetchedObjects firstObject];
        jobCategoryForUpdate = jobCategory;
        
        NSError *error = nil;
        if ([[self managedObjectContext] hasChanges]) {
            if (![[self managedObjectContext] save:&error]) {
                NSLog(@"Save Failed: %@", [error localizedDescription]);
            } else {
                NSLog(@"Save succeeded");
            }
        };
    }
}


- (NSArray *)getJobsForSelectedCounties
{
    NSMutableArray *allJobsArray = [[NSMutableArray alloc] init];
    
    NSArray *allCounties = [self getCounties];
    
    for (County *county in allCounties)
    {
        if ([[county countySelected] isEqual:@1])
        {
            [allJobsArray addObjectsFromArray:[county.toJob allObjects]];
        }
    }

    return allJobsArray;
}


- (NSArray *)getJobsForSelectedCategories
{
    NSMutableArray *allJobsArray = [[NSMutableArray alloc] init];
    
    NSArray *allCategories = [self getJobCategories];
    
    for (JobCategory *jobCategory in allCategories)
    {
        if ([[jobCategory categorySelected] isEqual:@1])
        {
            [allJobsArray addObjectsFromArray:[jobCategory.toJob allObjects]];
        }
    }
    
    return allJobsArray;
}


- (NSArray *)getSelectedJobs
{
    NSMutableArray *allSelectedJobs = [[NSMutableArray alloc] init];
    NSArray *finalArray = [[NSArray alloc] init];
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"jobTitle" ascending:YES];
    
    if ([[self getJobsForSelectedCounties] count] == 0)
    {
        [allSelectedJobs addObjectsFromArray:[self getJobsForSelectedCategories]];
    }
    else if ([[self getJobsForSelectedCategories] count] == 0)
    {
        [allSelectedJobs addObjectsFromArray:[self getJobsForSelectedCounties]];
    }
    else
    {
        for (Job *job in [self getJobsForSelectedCounties])
        {
            for (JobCategory *jobCategory in [self getSelectedJobCategories])
            {
                if (job.toJobCategory == jobCategory)
                {
                    [allSelectedJobs addObject:job];
                }
            }
        }
    }

    finalArray = [allSelectedJobs sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
    
    return finalArray;
}


- (NSArray *)getSelectedJobsWithSortingOption:(NSString *)option andAscending:(BOOL)ascending andSearchString:(NSString *)string
{
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:option ascending:ascending];
    
    NSArray *currentArray = [[self getSelectedJobs] sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
    
    
    if ([string length] == 0)
    {
        return currentArray;
    }
    
    string = [string uppercaseString];
    string = [string stringByReplacingOccurrencesOfString:@"Č" withString:@"C"];
    string = [string stringByReplacingOccurrencesOfString:@"Ć" withString:@"C"];
    string = [string stringByReplacingOccurrencesOfString:@"Š" withString:@"S"];
    string = [string stringByReplacingOccurrencesOfString:@"Ž" withString:@"Z"];
    string = [string stringByReplacingOccurrencesOfString:@"Đ" withString:@"D"];
    
    
    NSMutableArray *arrayOfJobs = [[NSMutableArray alloc] init];
    
    for (Job *job in currentArray)
    {
        if ([job.jobKeyword rangeOfString:string].length > 0 || [job.jobLocationKeyword rangeOfString:string].length > 0)
        {
            [arrayOfJobs addObject:job];
        }
    }
    
    return arrayOfJobs;
}


- (void)performFullSync
{
    NSArray *allCounties = [[self getCounties] copy];
    NSArray *allJobCategories = [[self getJobCategories] copy];
    
    for (County *county in allCounties)
    {
        [[Parser sharedParser] parseDataForCounty:county];
    }
                    
    for (JobCategory *jobCategory in allJobCategories)
    {
        [[Parser sharedParser] parseDataForJobCategory:jobCategory];
    }
}


- (void)performSelectedSync
{
    for (County *county in [self getSelectedCounties])
    {
        [[Parser sharedParser] parseDataForCounty:county];
    }
    
    for (JobCategory *jobCategory in [self getSelectedJobCategories])
    {
        [[Parser sharedParser] parseDataForJobCategory:jobCategory];
    }
}


- (void)jobSeen:(Job *)job
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Job"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"jobID == %@", job.jobID];
    [fetch setPredicate:predicate];
    
    
    NSError *errorForHistory = nil;
    
    NSFetchRequest *fetchForHistory = [NSFetchRequest fetchRequestWithEntityName:@"History"];
    NSArray *historyObjects = [[self managedObjectContext] executeFetchRequest:fetchForHistory error:&errorForHistory];
    
    
    NSError *error = nil;
    
    NSArray *jobObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    if ([jobObjects count])
    {
        [[jobObjects firstObject] setJobSeenDate:[NSDate date]];
        [[jobObjects firstObject] setJobSeen:@1];
        [[jobObjects firstObject] setToHistory:[historyObjects firstObject]];
        
        NSError * error = nil;
        if (![[self managedObjectContext] save:&error])
        {
            NSLog(@"Error ! %@", error);
        }
    }
}



- (void)addFavoriteToJob:(Job *)job
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Job"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"jobID == %@", job.jobID];
    [fetch setPredicate:predicate];
    
    NSError *errorForFavorites = nil;
    
    NSFetchRequest *fetchForFavorite = [NSFetchRequest fetchRequestWithEntityName:@"Favorite"];
    NSArray *favoriteObjects = [[self managedObjectContext] executeFetchRequest:fetchForFavorite error:&errorForFavorites];
    
    NSError *error = nil;
    
    NSArray *jobObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    [jobObjects.firstObject setToFavorite:[favoriteObjects firstObject]];
    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
}


- (void)removeFavoriteJob:(Job *)job
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Job"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"jobID == %@", job.jobID];
    [fetch setPredicate:predicate];
    
    NSError *error = nil;
    
    NSArray *jobObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    [jobObjects.firstObject setToFavorite:nil];
    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
}


- (NSArray *)getFavorites
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Favorite"];
    
    NSError *error = nil;
    
    NSArray *favoriteObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"jobTitle" ascending:YES];
    return [[[favoriteObjects firstObject] toJob] sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
}


- (void)checkForExpiredJobs
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Job"];
    
    NSError *error = nil;
    
    NSArray *allJobs = [[self managedObjectContext] executeFetchRequest:fetch error:&error];

    for (Job *job in allJobs)
    {
        if (job.jobEndDate != nil)
        {
            if ([self daysBetweenDate:job.jobEndDate andDate:[NSDate date]] > 0)
            {
                [[self managedObjectContext] deleteObject:job];
                NSLog(@"DELETED - %@", job.jobTitle);
            }
        }
    }
    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
}


- (NSArray *)getHistoryJobs
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"History"];
    
    NSError *error = nil;
    
    NSArray *historyObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"jobSeenDate" ascending:NO];
    return [[[historyObjects firstObject] toJob] sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
}



- (void)deleteHistory
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"History"];
    
    NSError *error = nil;
    
    NSArray *historyObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    [[self managedObjectContext] deleteObject:[historyObjects firstObject]];
    
    NSEntityDescription *entityHistory = [NSEntityDescription entityForName:@"History" inManagedObjectContext:[self managedObjectContext]];
    __unused History *history = [[History alloc]initWithEntity:entityHistory insertIntoManagedObjectContext:[self managedObjectContext]];

    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"HistoryDeleted" object:nil]];
    }
}



- (void)deleteFavorites
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Favorite"];
    
    NSError *error = nil;
    
    NSArray *favoriteObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    [[self managedObjectContext] deleteObject:[favoriteObjects firstObject]];
    
    NSEntityDescription *entityFavorite = [NSEntityDescription entityForName:@"Favorite" inManagedObjectContext:[self managedObjectContext]];
    __unused Favorite *favorite = [[Favorite alloc]initWithEntity:entityFavorite insertIntoManagedObjectContext:[self managedObjectContext]];
    
    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"FavoritesDeleted" object:nil]];
    }
}


- (void)deleteAllCountiesSelection
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"County"];
    
    NSError *error = nil;
    
    NSArray *countyObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    for (County *county in countyObjects)
    {
        county.countySelected = @0;
        county.countyLoadedTime = nil;
    }
    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
    
    _fetchedResultsController = nil;
}



- (void)deleteAllCategoriesSelection
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"JobCategory"];
    
    NSError *error = nil;
    
    NSArray *categoriesObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    for (JobCategory *jobCategory in categoriesObjects)
    {
        jobCategory.categorySelected = @0;
        jobCategory.categoryLoadedTime = nil;
    }
    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
    
    _fetchedResultsController = nil;
}


- (void)deleteCache
{
    NSFileManager *filemgr;
    
    [self deleteHistory];
    [self deleteAllJobs];
    [self deleteAllCountiesSelection];
    [self deleteAllCategoriesSelection];
    
    filemgr = [NSFileManager defaultManager];
    
    if ([filemgr removeItemAtPath: [NSHomeDirectory() stringByAppendingString:@"/Library/Caches"] error: NULL]  == YES)
    {
        NSLog (@"Remove successful");
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"CacheDeleted" object:nil]];
        
    }
    else
    {
        NSLog (@"Remove failed");
    }
    
    [filemgr createDirectoryAtPath: [NSHomeDirectory() stringByAppendingString:@"/Library/Caches"] withIntermediateDirectories:NO attributes:nil error:nil];
}


- (void)deleteAllJobs
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Job"];
    
    NSError *error = nil;
    
    NSArray *jobObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    for (Job *job in jobObjects)
    {
        [[self managedObjectContext] deleteObject:job];
    }
    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
}


- (NSInteger)daysBetweenDate:(NSDate *)firstDate andDate:(NSDate *)secondDate
{
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [currentCalendar components: NSDayCalendarUnit
                                                      fromDate: firstDate
                                                        toDate: secondDate
                                                       options: 0];
    
    NSInteger days = [components day];
    return days;
}



- (void)deleteJob:(Job *)job
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Job"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"jobID == %@", job.jobID];
    [fetch setPredicate:predicate];
    
    NSError *error = nil;
    
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    
    for (Job *job in fetchedObjects)
    {
        [[self managedObjectContext] deleteObject:job];
    }
    
    NSError * error2 = nil;
    if (![[self managedObjectContext] save:&error2])
    {
        NSLog(@"Error ! %@", error2);
    }
}



@end
