//
//  AppDelegate.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "AppDelegate.h"
#import "CoreDataController.h"
#import "MMDrawerController.h"
#import "MMDrawerVisualState.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "DataSource.h"
#import "Reachability.h"
#import "RNCachingURLProtocol.h"
#import <LReachabilityController.h>
#import "EVURLCache.h"
#import <Parse/Parse.h>

@interface AppDelegate ()



@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[LReachabilityController sharedReachabilityController] startReachability];
    [CoreDataController sharedCDController];
    [DataSource sharedDataSource];
    [NSURLProtocol registerClass:[RNCachingURLProtocol class]];
    
    [Parse setApplicationId:@"SDxvv67yNMXHVKMZ4V0GGfjMO4cFWIp00dnySevG"
                  clientKey:@"F848Iz2vun38fisLQ5JW4jF4tdzcJZSqStC5ZE1D"];
    
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    MenuViewController * leftSideDrawerViewController = [[MenuViewController alloc] init];
    HomeViewController * centerViewController = [[HomeViewController alloc] init];
    
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:centerViewController];
    self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:navigationController
                             leftDrawerViewController:leftSideDrawerViewController
                             rightDrawerViewController:nil];
    
    centerViewController.drawerController = self.drawerController;
    [self.drawerController setShowsShadow:YES];
    self.drawerController.maximumLeftDrawerWidth = 270.0;
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    [self.drawerController setDrawerVisualStateBlock:[MMDrawerVisualState parallaxVisualStateBlockWithParallaxFactor:2.0]];
    self.drawerController.shouldStretchDrawer = YES;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self applyStyle];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"runOnce"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DetailedSelected"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"runOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self.window setRootViewController:self.drawerController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    return YES;
}


- (void)applyStyle
{
    UIColor * tintColor = [UIColor colorWithRed:29.0/255.0
                                          green:173.0/255.0
                                           blue:234.0/255.0
                                          alpha:1.0];
    

    [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
        
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:180/255.0 green:180/255.0 blue:180/255.0 alpha:1.0]];
        
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                               [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                               shadow, NSShadowAttributeName,
                                                               [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:21.0], NSFontAttributeName, nil]];
        
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        
    [self.window setTintColor:tintColor];
}


- (void) reachabilityChanged:(NSNotification*) notification
{
    Reachability* reachability = notification.object;
    
    if(reachability.currentReachabilityStatus == NotReachable)
        NSLog(@"Internet off");
    else
        NSLog(@"Internet on");
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}






@end