//
//  History.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 16/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Job;

@interface History : NSManagedObject

@property (nonatomic, retain) NSSet *toJob;
@end

@interface History (CoreDataGeneratedAccessors)

- (void)addToJobObject:(Job *)value;
- (void)removeToJobObject:(Job *)value;
- (void)addToJob:(NSSet *)values;
- (void)removeToJob:(NSSet *)values;

@end
