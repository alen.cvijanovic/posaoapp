//
//  County.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "County.h"
#import "Job.h"


@implementation County

@dynamic countyTitle;
@dynamic countyURL;
@dynamic countySelected;
@dynamic countyLoaded;
@dynamic countyID;
@dynamic countyLoadedTime;
@dynamic toJob;

@end
