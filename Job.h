//
//  Job.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class County, Favorite, JobCategory, History;

@interface Job : NSManagedObject

@property (nonatomic, retain) NSDate * jobEndDate;
@property (nonatomic, retain) NSString * jobKeyword;
@property (nonatomic, retain) NSString * jobLocation;
@property (nonatomic, retain) NSString * jobLocationKeyword;
@property (nonatomic, retain) NSDate * jobStartingDate;
@property (nonatomic, retain) NSString * jobTitle;
@property (nonatomic, retain) NSString * jobID;
@property (nonatomic, retain) NSString * jobURL;
@property (nonatomic, retain) NSNumber * jobSeen;
@property (nonatomic, retain) NSDate * jobSeenDate;
@property (nonatomic, retain) County *toCounty;
@property (nonatomic, retain) Favorite *toFavorite;
@property (nonatomic, retain) JobCategory *toJobCategory;
@property (nonatomic, retain) History *toHistory;

@end
