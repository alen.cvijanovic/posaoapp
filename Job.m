//
//  Job.m
//  Nadji Posao
//
//  Created by Alen Cvijanović on 02/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import "Job.h"
#import "County.h"
#import "Favorite.h"
#import "JobCategory.h"


@implementation Job


@dynamic jobEndDate;
@dynamic jobKeyword;
@dynamic jobLocation;
@dynamic jobLocationKeyword;
@dynamic jobStartingDate;
@dynamic jobTitle;
@dynamic jobID;
@dynamic jobURL;
@dynamic jobSeen;
@dynamic jobSeenDate;
@dynamic toCounty;
@dynamic toFavorite;
@dynamic toJobCategory;
@dynamic toHistory;

@end
