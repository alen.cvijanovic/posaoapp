//
//  JobCategory.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Job;

@interface JobCategory : NSManagedObject

@property (nonatomic, retain) NSString * categoryTitle;
@property (nonatomic, retain) NSString * categoryURL;
@property (nonatomic, retain) NSString * categoryID;
@property (nonatomic, retain) NSNumber * categorySelected;
@property (nonatomic, retain) NSNumber * categoryLoaded;
@property (nonatomic, retain) NSDate * categoryLoadedTime;
@property (nonatomic, retain) NSSet *toJob;
@end

@interface JobCategory (CoreDataGeneratedAccessors)

- (void)addToJobObject:(Job *)value;
- (void)removeToJobObject:(Job *)value;
- (void)addToJob:(NSSet *)values;
- (void)removeToJob:(NSSet *)values;

@end
