//
//  County.h
//  Nadji Posao
//
//  Created by Alen Cvijanović on 01/06/15.
//  Copyright (c) 2015 Alen Cvijanovic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Job;

@interface County : NSManagedObject

@property (nonatomic, retain) NSString * countyTitle;
@property (nonatomic, retain) NSString * countyURL;
@property (nonatomic, retain) NSString * countyID;
@property (nonatomic, retain) NSNumber * countySelected;
@property (nonatomic, retain) NSNumber * countyLoaded;
@property (nonatomic, retain) NSDate * countyLoadedTime;
@property (nonatomic, retain) NSSet *toJob;
@end

@interface County (CoreDataGeneratedAccessors)

- (void)addToJobObject:(Job *)value;
- (void)removeToJobObject:(Job *)value;
- (void)addToJob:(NSSet *)values;
- (void)removeToJob:(NSSet *)values;

@end
